// Image Overlay Layers

var imageNameLayer = L.layerGroup([imgNameOverlay]);

// Define layer groups

var overlayLayers = {
  "Reference": refLayer,
  "Towns": townLayer,
  "Outposts": outpostLayer,
  "Ruins": ruinLayer,
  "Villages": villageLayer,
  "Slave Camps": slaveCampLayer,
  "Points of Interest": poiLayer,
  // "Bounties": bountyLayer,
  "Zone Names": imageNameLayer,
  "Custom Markers": customLayer
};

// Add Layer control containing layer groups to map
L.control.layers(null, overlayLayers).addTo(myMap);
L.control.layers(baseMaps, null).addTo(myMap);

// Remove the Leaflet logo in the bottom right
document.getElementsByClassName('leaflet-control-attribution')[0].style.display = 'none';
